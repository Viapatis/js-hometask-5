# js-hometask-5

## Ответьте на вопросы

1. Какие данные хранятся в следующих свойствах узла: `parentElement`, `children`, `previousElementSibling` и `nextElementSibling`?
    parentElement родительский Element <br />
    children коллекция дочерних Element(ов) <br />
    previousElementSibling дочерний Element стоящий в дереве перед текущим или null если такого нет <br />
    nextElementSibling     дочерний Element стоящий в дереве после текущего или null если такого нет <br />
2. Нарисуй какое получится дерево для следующего HTML-блока.

```html
<p>Hello,<!--MyComment-->World!</p>
```
<p>
![citilink.ru](./Diagram.svg)<br />
## Выполните задания

1. Клонируй репозиторий;
2. Допиши функции в `tasks.js`;
3. Для проверки работы функций открой index.html в браузере;
4. Создай MR с решением.
