'use strict';
/*
    В этом задании нельзя менять разметку, только писать код в этом файле.
 */

/**
*   1. Удали со страницы элемент с id "deleteMe"
**/

function removeBlock() {
    document.getElementById('deleteMe').remove();
}

/**
 *  2. Сделай так, чтобы во всех элементах с классом wrapper остался только один параграф,
 *  в котором будет сумма чисел из всех параграфов.
 *
 *  Например, такой элемент:
 *
 *  <div class="wrapper"><p>5</p><p>15</p><p>25</p><p>35</p></div>
 *
 *  должен стать таким
 *
 *  <div class="wrapper"><p>80</p></div>
 */

function calcParagraphs() {
    [...document.getElementsByClassName('wrapper')].forEach((wrapper) => {
        const pArr = wrapper.getElementsByTagName('p');
        while ( pArr.length>1) {
            pArr[0].textContent=+pArr[0].textContent+ +pArr[1].textContent;
            pArr[1].remove();
        }
    })
}

/**
 *  3. Измени value и type у input c id "changeMe"
 *
 */

function changeInput() {
    const input = document.getElementById('changeMe');
    input.setAttribute('type', 'text');
    input.setAttribute('value', 'Hello World');
}

/**
 *  4. Используя функции appendChild и insertBefore дополни список с id "changeChild"
 *  чтобы получилась последовательность <li>0</li><li>1</li><li>2</li><li>3</li>
 *
 */

function appendList() {
    const ul = document.getElementById('changeChild');
    const lis = ul.getElementsByTagName('li');
    const maxCount = 3;
    if (lis[lis.length - 1].textContent != maxCount) {
        const newLi = document.createElement('li');
        newLi.textContent = maxCount;
        ul.appendChild(newLi);
    }
    for (let i = 1; i < lis.length; i++) {
        const currentLiVal = +lis[i].textContent,
            beforeLiVal = +lis[i - 1].textContent;
        for (let j = 0; j < currentLiVal - beforeLiVal - 1; j++) {
            const newLi = document.createElement('li');
            newLi.textContent = beforeLiVal + 1;
            ul.insertBefore(newLi, lis[i]);
        }
    }
}

/**
 *  5. Поменяй цвет всем div с class "item".
 *  Если у блока class "red", то замени его на "blue" и наоборот для блоков с class "blue"
 */

function changeColors() {
    [...document.getElementsByClassName('item')].forEach((item) => {
        const divClass = item.classList.contains('red') ? 'red' : 'blue';
        item.classList.remove(divClass);
        item.classList.add(divClass === 'blue' ? 'red' : 'blue');
    })
}

removeBlock();
calcParagraphs();
changeInput();
appendList();
changeColors();
